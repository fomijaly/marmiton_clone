import MainHeader from './MainHeader'
import './Header.css'
import Navbar from './Navbar'

function Header(){
    return(
        <>
            <main className='main_header'>
                <MainHeader />
                <Navbar />
            </main>
        </>
    )
}

export default Header