import './RecipeEnCeMoment.css'

function RecipeEnCeMoment(recipeMoment){
    return(
        <>
            <article className={"recipes_moment_image " + recipeMoment.class}>
                <section className="detail_recipe">
                    <p>{recipeMoment.description}</p>
                </section>
                <span><i className={"fa " + recipeMoment.iconClass}></i></span>
            </article>
        </>
    )
}

export default RecipeEnCeMoment