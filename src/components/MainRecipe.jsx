import './MainRecipe.css'

function MainRecipe(mainrecipe) {
    return(
        <>
            <article className='recette_une_box'>
                <img className="recette_une_image" src={mainrecipe.imageSrc} alt={mainrecipe.name} />
                <span>{mainrecipe.tag}</span>
                <p>{mainrecipe.description}</p>
            </article>
        </>
    )
}

export default MainRecipe