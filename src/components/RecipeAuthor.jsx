import './RecipeAuthor.css'

function RecipeAuthor(author){
    return (
        <>
            <section className="author_box">
                <img src={author.image} alt={author.name} />
                <p className='author_name'>{author.name}</p>
            </section>
        </>
    )
}

export default RecipeAuthor