import './AdvicedRecipe.css'
import RecipeAuthor from './RecipeAuthor'

function AdvicedRecipe(advicedrecipe){
    return (
        <>
            <div className='adviced_recipe_box'>
                <img className='adviced_recipe_image' src={advicedrecipe.image} alt={advicedrecipe.name} />
                <div className='adviced_recipe_details'>
                    <span>{advicedrecipe.tag}</span>
                    <p className='adviced_recipe_description'>{advicedrecipe.description}</p>
                    <RecipeAuthor name={advicedrecipe.author} image={advicedrecipe.authorPicture}/>
                </div>
            </div>
        </>
    )
}

export default AdvicedRecipe