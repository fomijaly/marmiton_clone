import "./FastRecipe.css"

function FastRecipe(fastrecipe){
    return(
        <>
            <article className="fast_recipe_box">
                <img src="https://assets.afcdn.com/homepage/folderBlock/642853592_w964h574c1.webp" alt="" />
                <div className="fast_recipe_details">
                    <h2 className="fast_recipe_title">{fastrecipe.name}</h2>
                    <p className="fast_recipe_description">{fastrecipe.description}</p>
                    <button className="fast_recipe_button">Accéder au dossier</button>
                </div>
            </article>
        </>
    )
}

export default FastRecipe