import { useState } from 'react'
import './Recipe.css'


function Recipe(recipe){
    const {name, addClass, rate, reviews} = recipe;
    const [isLiked, setIsLiked] = useState(false);
    return(
        <>
            <article className={`card_recipe ${addClass}`}>
                <section className='details_recipe'>
                    <div className='details_recipe_box'>
                        <span>{name}</span>
                        <p>{rate !== 0 ? rate + " / 5" : "Aucune note"}</p>
                        <p>{reviews !== 0 ? reviews + " avis" : "Aucun avis"}</p>
                    </div>
                </section>
                <button onClick={() => setIsLiked(isLiked => !isLiked)}>{isLiked ? <i className='fa fa-heart is_liked'></i> : <i className='fa fa-heart is_unliked'></i>}</button>
            </article>
        </>
    )
}

export default Recipe