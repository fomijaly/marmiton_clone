import './RecipeActus.css'

function RecipeActus(recipeActus){
    return(
        <>
            <article className='recipe_actus_box'>
                <div className="recipe_actus_image_box">
                    <img className='recipe_actus_image' src={recipeActus.imageSrc} alt={recipeActus.name} />
                    <span><i className="fa fa-bars-staggered icon_recipe"></i></span>
                </div>
                <div className='recipe_actus_details'>
                    <p>{recipeActus.description}</p>
                    <span>{recipeActus.dateParution}</span>
                </div>
            </article>
        </>
    )
}

export default RecipeActus