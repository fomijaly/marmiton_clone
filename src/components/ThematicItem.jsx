import './ThematicItem.css'

function ThematicItem(thematicItem){
    return(
        <>
            <article className='thematic_item'>
                <span className={thematicItem.imageClass}> </span>
                <p>{thematicItem.name}</p>
            </article>
        </>
    )
}

export default ThematicItem