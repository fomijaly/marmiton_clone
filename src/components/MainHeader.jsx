import './MainHeader.css'

function MainHeader(){
    return(
        <>
            <main className='search_header'>
                <button className='button_menu'><i className="fa fa-bars"></i></button>
                <a href="#" className='logo_marmiton'></a>
                <form>
                    <button className="button_search_left" type='submit'><i className="fa fa-magnifying-glass"></i></button>
                    <label htmlFor="search">Je cherche</label>
                    <input className="search_input" type="text" name="search" placeholder="une recette, un ingrédient, de l'aide"/>
                    <button className="button_search_right" type='submit'><i className="fa fa-magnifying-glass"></i></button>
                </form>
                <button className='button_user'><i className="fa fa-user"></i>Connexion</button>
            </main>
        </>
    )
}

export default MainHeader