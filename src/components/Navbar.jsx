import './Navbar.css'

function Navbar() {
    return(
        <>
            <nav className='navbar_header'>
                <ul>
                    <li>Le magazine</li>
                    <li>Noël festif et malin</li>
                    <li>Recettes ultra rapides</li>
                    <li>Actus</li>
                    <li>Mieux manger</li>
                    <li>Prix Marmiton 2023</li>
                </ul>
            </nav>
        </>
    )
}

export default Navbar