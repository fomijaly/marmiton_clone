import './HelpRecipe.css'

function HelpRecipe(helpRecipe){
    return(
        <>
            <div className='help_recipe_image_box'>
                <img src={helpRecipe.image} alt={helpRecipe.name}/>
                <span><i className={"fa " + helpRecipe.iconClass}></i></span>
                <p>{helpRecipe.description}</p>
            </div>
        </>
    )

}

export default HelpRecipe