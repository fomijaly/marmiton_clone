const recipeOfDay = [
    {
        name: "Quinoa au curry",
        addClass: "quinoa_curry",
        rate: 4.5,
        reviews: 62,
    },
    {
        name: "Clémentines chocolat pistaches",
        addClass: "clementine_pistache",
        rate: 4.9,
        reviews: 11,
    },
    {
        name: "Gaufres aux trois fromages et pesto",
        addClass: "gaufres_pesto",
        rate: 0,
        reviews: 0,
    },
];

const recipesActual = [
    {
        class: "gratin_courgette", 
        description: "Comment éviter que le gratin de courgettes rende plein d'eau ?", 
        iconClass: "fa-bars-staggered"
    },
    {
        class: "courgettes_economiques", 
        description: "Nos recettes délicieuses et économiques avec des courgettes", 
        iconClass: "fa-clone"
    },
    {
        class: "idee_grillades", 
        description: "Nos idées de grillades pour petit budget", 
        iconClass: "fa-clone"
    },
    {
        class: "recettes_familiales_sans_viande", 
        description: "30 recettes sans viande et familiales pour ce soir", 
        iconClass: "fa-clone"
    },
]

export {recipeOfDay, recipesActual};