import {recipeOfDay, recipesActual} from './data.js'

import './App.css'
import './styles-767px.css'
import Recipe from './components/Recipe'
import MainRecipe from './components/MainRecipe'
import AdvicedRecipe from './components/AdvicedRecipe'
import RecipeActus from './components/RecipeActus'
import RecipeEnCeMoment from './components/RecipeEnCeMoment'
import FastRecipe from './components/FastRecipe'
import HelpRecipe from './components/HelpRecipe'
import ThematicItem from './components/ThematicItem'

function App() {

  return (
    <>
      <main className='card'>
        <section className='recipes_boxes'>
          <h2>A la une de Marmiton</h2>
          <div className='main_recipes_box'>
            <article className='main_recipes'>
              <MainRecipe name="Recette de compote à la pomme" tag="RECETTES" description="« Une superbe recette, rien à ajouter, il faut tout suivre. » : voici LA meilleure recette de compote de pommes Marmiton" imageSrc="https://assets.afcdn.com/story/20231120/2235041_w1200h760c1.webp"/>
            </article>
            <article className='advices'>
              <AdvicedRecipe name="Soldes d'été: Tefal" image="https://assets.afcdn.com/story/20230630/2222743_w320h320c1.webp" tag="MIEUX ACHETER" description="Soldes d’été 2023 : Tefal, Le Creuset, Laguiole, jusqu’à - 50% sur les cocottes en fonte" author="Sarah Checkroun" authorPicture="https://assets.afcdn.com/manage/redacteur/Sarah.Chekroun_w312h312c1.webp"/>
              <hr />
              <AdvicedRecipe name="Alerte œufs pollués" image="https://assets.afcdn.com/story/20231121/2235127_w320h320c1.webp" tag="MIEUX MANGER" description="Alerte œufs pollués : l'Agence Régionale de Santé recommande de ne plus consommer ceux issus de ces poulaillerse" author="Adeline Huar" authorPicture="https://assets.afcdn.com/manage/redacteur/1653814899_w312h312c1.webp"/>
              <hr />
              <AdvicedRecipe name="Bananes courbées" image="https://assets.afcdn.com/story/20231117/2234842_w320h320c1.webp" tag="CUISINE DU QUOTIDIEN" description="Savez-vous pourquoi les bananes sont courbées? " author="Adeline Huar" authorPicture="https://assets.afcdn.com/manage/redacteur/1653814899_w312h312c1.webp"/>
            </article>
          </div>
        </section>
        <section className='recipes_boxes'>
          <h2>Les recettes du jour</h2>
          <div className="carousel">
            <div className='recettes_du_jour'>
              {recipeOfDay.map((currentElement, index) => (
                <Recipe key={index} {...currentElement}/>
                ))}
            </div>
          </div>
        </section>
        <section className="recipes_boxes">
          <div className='recipes_actus-title'>
            <h2>Consultez nos dernières actus</h2>
            <span className="format-basic">Voir plus <i className='fa fa-arrow-circle-right'></i></span>
            <span className="format-responsive"><i className='fa fa-arrow-circle-right'></i></span>
          </div>
          <div className='dernieres_actus'>
            <div className='recipes_actus_box-row'>
              <RecipeActus name="Remplacez l'avocat" imageSrc="https://assets.afcdn.com/story/acc9_4075353/acc516588725_w240h240c1cxt0cyt0cxb1200cyb630.webp" description="Vous n'avez plus d'avocat pour préparer un guacamole ? Remplacez-le par cet ingrédient de saison et pas cher !" dateParution="Il y a quelques minutes"/>
              <RecipeActus name="Recette des biscuits de noël" imageSrc="https://assets.afcdn.com/story/acc9_4075374/acc1937929011_w240h240c1cxt0cyt0cxb2110cyb1366.webp" description='"Réalisée depuis des années et toujours parfaite" : voici LA meilleure recette de biscuits de Noël Marmiton' dateParution="Il y a quelques minutes"/>
            </div>
            <hr />
            <div className='recipes_actus_box-row'>
              <RecipeActus name="Black Friday: Blender" imageSrc="https://assets.afcdn.com/story/acc9_4075400/acc1837997977_w240h240c1cxt0cyt0cxb1200cyb630.webp" description="Black Friday 2023 : économisez 50€ sur le célèbre blender chauffant Easy Soup de Moulinex !" dateParution="Il y a quelques minutes"/>
              <RecipeActus name="Aliments moisis" imageSrc="https://assets.afcdn.com/story/acc9_4075364/acc121684765_w240h240c1cxt0cyt0cxb8688cyb5792.webp" description="Le saviez-vous ? Ces aliments pourront toujours être consommés même moisis, selon cette virologiste" dateParution="Aujourd'hui"/>
            </div>
            <hr />
            <div className='recipes_actus_box-row'>
              <RecipeActus name="Charlotte à la mangue - Cyril Lignac" imageSrc="https://assets.afcdn.com/story/acc9_4075357/acc32523465_w240h240c1cxt0cyt0cxb6000cyb4000.webp" description="Cyril Lignac partage sa recette de verrines façon charlotte à la mangue parfaite pour les fêtes de fin d'année" dateParution="Il y a quelques minutes"/>
              <RecipeActus name="Gingembre et sushis" imageSrc="https://assets.afcdn.com/story/acc9_4075376/acc2129200777_w240h240c1cxt0cyt0cxb2000cyb1333.webp" description="Voilà quoi sert le gingembre qui accompagne vos sushis (spoiler : ce n’est pas une question de goût)" dateParution="Aujourd'hui"/>
            </div>
          </div>
        </section>
        <section className='recipes_boxes'>
          <h2>En ce moment</h2>
          <div className="carousel">
            <div className='recipes_moment'>
              {recipesActual.map((currentElement, index) => (
                  <RecipeEnCeMoment key={index} {...currentElement}/>
              ))}
            </div>
          </div>
        </section>
        <section className='recipes_boxes recipe_help_box'>
          <span className='help_box_circle help_circle_right'></span>
          <span className='help_box_circle help_circle_top'></span>
          <span className='help_box_circle help_circle_left'></span>
          <div className='fast_recipe_box'>
            <FastRecipe name="Cuisine Rapide" description="Des idées pour trouver la bonne recette rapide à faire pour ce soir !"/>
          </div>
          <div className='help_recipes_box'>
            <h3>Pour vous aider en cuisine</h3>
            <div className='help_recipe_items'>
              <HelpRecipe name="recettes_pate_feuilletee" image="https://assets.afcdn.com/album/D20230112/phalbm26120820_w400h400c1.webp" description="13 recettes originales et rapides à faire avec de la pâte feuilletée" iconClass="fa-clone"/>
              <HelpRecipe name="apero_improvise" image="https://assets.afcdn.com/story/20210624/2125937_w1888h1060c1cx765cy1363cxt0cyt559cxb1558cyb1707.webp" description="Comment faire un apéro improvisé?" iconClass="fa-bars-staggered"/>
              <HelpRecipe name="aperitif_peu_ingredient" image="https://assets.afcdn.com/album/D20200615/phalbm25938806_w400h400c1.webp" description="20 idées d’apéritifs avec peu d’ingrédients?" iconClass="fa-clone"/>
              <HelpRecipe name="recettes_coktails" image="https://assets.afcdn.com/album/D20170915/phalbm25238705_w400h400c1.webp" description="Les 33 meilleures recettes de cocktails pour vos soirées et vos apéros" iconClass="fa-clone"/>
            </div>
          </div>
        </section>
        <section className='inspirations_thematiques'>
          <span className='thematic_box_circle thematic_circle_left'></span>
          <span className='thematic_box_circle thematic_circle_middle'></span>
          <span className='thematic_box_circle thematic_circle_right'></span>
          <h2>Inspiration thématique</h2>
          <div className='thematics'>
            <ThematicItem name="Rapide" imageClass="thematic_item_rapide"/>
            <ThematicItem name="Entrées" imageClass="thematic_item_entrees"/>
            <ThematicItem name="Plats" imageClass="thematic_item_plats"/>
            <ThematicItem name="Desserts" imageClass="thematic_item_desserts"/>
            <ThematicItem name="Apéritifs" imageClass="thematic_item_aperitifs"/>
            <ThematicItem name="Sans viande" imageClass="thematic_item_meatless"/>
          </div>
        </section>
      </main>
    </>
  )
}

export default App
